﻿---
title:   > 
  \textsc{Pic 10c: Advanced Programming}
author:  Ricardo Salazar
date:    Winter 2018 (Lec 2)
header-includes:
  - \usepackage{caption}
---

## About the `.pdf` version of this syllabus

If you are reading the online [markdown] version of this document,
[click here][pdf] to access the location where the `.pdf` version is hosted;
then click on the `view raw` link to download a copy of this document.[^if-you]

[pdf]: readme.md.pdf
[^if-you]: The embedded link does not work in the `.pdf` document.


## Course information

**Contact:** [`rsalazar@math.ucla.edu`][correo] (write "Pic 10C" in the
subject).

**Time:** 12:00 to 12:50 pm.

**Location:** MS 6201.

**Office hours:** [See class website (CCLE)][class-website].

**Teaching assistant(s):**

|**Section**| **T. A.**     |**Office**| **E-mail**                           |
|:---------:|:--------------|:--------:|:-------------------------------------|
| 2A        | Fu, Chuyuan   |MS 7601   | [`fuchuyuan@math.ucla.edu`][t-a] |

[correo]: mailto:rsalazar@math.ucla.edu
[class-website]: https://ccle.ucla.edu/course/view/18W-COMPTNG10C-2
[t-a]: mailto:fuchuyuan@math.ucla.edu


## Course Description

_Miscellaneous topics in computer programming:_ Version control, advanced
features of `C++`, `Qt` (cute). Other topics might be included depending on how
fast/slow the material is covered.


## Textbook

You are welcome to use your copy of Horstmann C. & Budd, T. A. Big `C++`, _2nd
Edition._ John Wiley and Sons, Inc. However, be advised that most of the
material presented during lecture can be found elsewhere. Whenever possible,
lecture notes and/or slides will be prepared, and if applicable, made available
to students.


## CCLE and MyUCLA

This course will use a password-protected Internet site on UCLA CCLE to post
links to course materials and announcements. These materials can include the
syllabus, handouts and Internet links referenced in class (hard copies of course
materials \underline{\bf will not} be made available in class). _Please consult
CCLE and this syllabus before sending me an email about a policy or procedure;
your question(s) might already be answered here._ In addition, you can check
your scores via the MyUCLA Gradebook. Please notice that it is your
responsibility to verify in a timely manner that the scores appearing on MyUCLA
are accurate.


## Midterms

One fifty-minute midterm will be given on
**Monday, February 12**
from
**12:00 to 12:50 pm**
at
**MS 6201**.
_There will be absolutely no makeup midterms under any circumstances._

## Final Exam

The final exam will be given on
**Wednesday, March 21**
from
**8:00 to 11:00 am**
at
**TBD** (click [here][final-tbd] for up-to-date information about the exam
location). **_Failure to take the final exam during this time will result in an
automatic F!_**

[final-tbd]: https://sa.ucla.edu/ro/public/soc/Results/ClassDetail?term_cd=18W&subj_area_cd=COMPTNG&crs_catlg_no=0010C+++&class_id=157052210&class_no=+002++

Make sure to bring your UCLA ID card to every exam. You will not be allowed to
consult books, notes, the internet, digital media or another student's exam.
Please turn off and put away any electronic devices during the entire duration
of the exam.
 
The midterm exam will be returned to you during discussion section and your TA
will go over the exam at that time. _Any questions regarding how the exam was
graded **must be submitted in writing** with your exam to the TA at the end of
section that day._ No regrade requests will be allowed afterwards regardless of
whether or not you attended section. Please get in touch with me if you
anticipate missing section due to a family emergency or a medical reason.


## Grading

_Grading method:_ Assignments, Midterm Exam, Final Exam, and Final Project.

Your final score in the class will be the maximum of the following two grading
breakdowns:

|                                                                                    |
|:----------------------------------------------------------------------------------:|
| (15% Assignments) + (25% Midterm 1) + (30% Final Exam) + (35% Final Project)[^one] | 
| or                                                                                 |
| (15% Assignments) + (50% Final Exam) + (35% Final Project)                         |

[^one]: Please notice that the weights in this breakdown do not add up to 100%.

_Letter grades:_  

|                       |                      |                    |
|:----------------------|:---------------------|:-------------------|
|                       | A (93.33% or higher) | A- (90% -- 93.32%) |
| B+ (86.66% -- 89.99%) | B (83.33% -- 86.65%) | B- (80% -- 83.32%) |
| C+ (76.66% -- 79.99%) | C (73.33% -- 76.65%) | C- (70% -- 73.32%) |

The remaining grades will be assigned at my own discretion. Please DO NOT
request exceptions.

**All grades are final when filed by the instructor on the Final Grade Report.**

## Policies and procedures

Once a homework assignment is posted (CCLE) you will have at least one week to
complete it. The actual due date might change but it will always be available
in the description of the assignment. You are encouraged to check CCLE on a
regular basis to find out about changes. You will upload the requested files to
our course's CCLE website before the date and time posted on CCLE. The
assignments will be due at the time the PIC Lab closes on the due date. There
will be about 3 homework assignments throughout the quarter. Please note that
since the number of assignments is low, and since their weight on your final
score is low as well, **your lowest homework score will not be dropped**. _In an
effort to be fair to all students, messages sent to my email address that
contain either `.cpp`, `.txt`, or  `.h` attachments will automatically be
deleted from my inbox._

You are responsible for naming your files correctly and you need to make sure
that you submitted them correctly. You are free to use your favorite software
but be warned that your code needs to be tested on several different platforms.
For reference, the computer(s) that I will use during this quarter are equipped
with
**Visual Studio 2015**,
as well as some recent version of
`g++`
and
`clang++`
(this latter compiler is the one that is used by **Xcode**). You are encouraged
to test your code in the PIC lab before the assignment is due and to keep the
master branch of your repositories in a clean state (_e.g.,_ no commented out
blocks, no log files, and definitely no binary files). If your code does not
compile on any of the computers I have available at the time I am grading, I
will make my best effort to assign you a fair score based on the code that I am
able to review.

Scores on homework assignments and exams might appear on the CCLE website,
however, these score will not be the official ones. Usually I ask the class
grader to partially grade some assignments after week 5, then upload these
grades to CCLE. At this point, if your score is less that 20/20, you should
attempt to fix it. **All of your assignments will be graded by myself after the
final exam.** These official scores as well as comments and/or observations
about your work will be reported to MyUCLA. 

You are encouraged to discuss aspects of the course with other students as well
as homework assignments with others in general terms (i.e., general ideas and
words are OK but code is NOT OK). If you need specific help with your programs
you may consult the TA or the instructor only. Do not copy or cite any part of
a program or document written by someone else (e.g., code found online).
Homework solutions are automatically monitored for copied code.


## Final project 

Your final project consists of a series of _'deliverables'_ starting at week 3.
You have 2 weeks to come up with an approved 6-week project. If by the end of
this time you have not settled on a project I will be happy to suggest one for
you. The idea behind this is to get you excited about the material. By now you
should have some expertise will likely be able to come up with a pretty cool
project. Just remember: _all projects need to be approved by me_.


## Accommodations

If you need any accommodations for a disability, please contact the UCLA
CAE[^two] [(`www.cae.ucla.edu`)][CAE]. Make sure to let me know as soon as
possible about necessary arrangements that need to be made.

[CAE]: http://www.cae.ucla.edu
[^two]: Center for Accessible Education

|                                                      |
|:----------------------------------------------------:|
| Course Syllabus Subject to Update by the Instructor. |

